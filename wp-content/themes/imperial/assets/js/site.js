
import Sliders from './_site/sliders';
import HeaderFixed from './_site/headerFixed';
import AnimationOnScroll from './_site/animationOnScroll';
import NavBtn from './_site/navBtn';
import LoadItems from './_site/loadItems';
import MasonryGallery from './_site/masonryGallery';
import Carousel from './_site/carousel';
import Preloader from './_site/preloader';

jQuery(function () {
	// Slick Sliders
	Sliders.init();

	// Header Fixed
	HeaderFixed.init();

	// AnimationOnScroll
	AnimationOnScroll.init();

	// NavBtn
	NavBtn.init();
	
	// LoadItems
	LoadItems.init();
	
	// MasonryGallery
	MasonryGallery.init();
	
	// Carousel
	Carousel.init();
	
	// Preloader
	Preloader.init();

	console.log('hello');
});
