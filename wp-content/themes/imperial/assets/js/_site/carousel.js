const $ = jQuery.noConflict();

'use strict';
const Carousel = {

	/*-------------------------------------------------------------------------------
		# Initialize
	-------------------------------------------------------------------------------*/
	init: function () {
		var carouselItem = $('.js-carousel-item');
		var carouselImg = $('.js-carousel-img');
		var carouselArray = [0, 1, 2, 3, 4];
		var counter = 0;
		var currentNumberImg = 1;

		var i = setInterval(function(){
			var currentItem = carouselItem.eq(carouselArray[counter]);
			var currentImgs = currentItem.find(carouselImg);
			currentImgs.removeClass('active');
			currentImgs.eq(currentNumberImg).addClass('active');
			
			counter++;
			
			// reset counter
			if (counter === carouselArray.length) {
				counter = 0;
				
				if (currentNumberImg == 0) {
					currentNumberImg = 1;
				} else {
					currentNumberImg = 0;
				}
			}
		}, 2000);
	}
};

export default Carousel;
