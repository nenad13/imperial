const $ = jQuery.noConflict();

'use strict';
const MasonryGallery = {
	/*-------------------------------------------------------------------------------
		# Initialize
	-------------------------------------------------------------------------------*/
	init: function () {
        var galleryContainer = $('.js-gallery-container');
		var galleryItem = '.js-gallery-item';
		
		var $grid = galleryContainer.imagesLoaded( function() {
			// init Masonry after all images have loaded
			$grid.masonry({
				// options...
				itemSelector: galleryItem,
			});
		});
	}
};

export default MasonryGallery;