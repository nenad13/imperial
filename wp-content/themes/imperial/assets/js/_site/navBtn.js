const $ = jQuery.noConflict();

'use strict';
const NavBtn = {
	/*-------------------------------------------------------------------------------
		# Initialize
	-------------------------------------------------------------------------------*/
	init: function () {
        var menuBtn = $('.js-menu-btn');
		var $body = $('body');
		var menuHasSub = $('.menu-item-has-children');
        var header = $('.js-header');
        var mainNav = $('.js-main-nav');
		var subIcon = $('.js-sub-icon');
        
		if ($(window).width() < 1200) {
			menuBtn.on('click', function(e) {
				e.stopPropagation();
				mainNav.slideToggle();
				menuBtn.toggleClass('open');
				header.toggleClass('header-open').toggleClass('header__color');
				$body.toggleClass('noscroll');
			});	
			
		
			menuHasSub.each(function (i, el) {
				$(el).append('<span class="sub-icon font-next js-sub-icon"></span>');
			});
		
			menuHasSub.on('click', '.js-sub-icon', function () {
				$(this).siblings('.sub-menu').slideToggle();
				$(this).toggleClass('icon-rotate');
			});
			
		}
		
	}
};

export default NavBtn;