const $ = jQuery.noConflict();

'use strict';
const AnimateOnScroll = {

    /*-------------------------------------------------------------------------------
    	# Initialize
    -------------------------------------------------------------------------------*/
    init: function() {
		var $window = $(window);

        function animateElInit(selector) {
            var $animationEl = selector;
            
            var visibleClass = 'is-visible';

            function checkIfInView() {
                var windowHeight = $window.height();
                var windowTopPosition = $window.scrollTop();
                var windowBottomPosition = (windowTopPosition + windowHeight);

                $animationEl.each(function() {
                    var $element = $(this);
                    var elementHeight = $element.outerHeight();
                    var elementTopPosition = $element.offset().top + windowHeight / 5;
                    var elementBottomPosition = (elementTopPosition + elementHeight);
                    var animationDelay = $(this).attr('data-slide-delay');

                    // check to see if this current container is within viewport
                    if ((elementBottomPosition >= windowTopPosition) && (elementTopPosition <= windowBottomPosition)) {
                        setTimeout(function() {
                            $element.addClass(visibleClass);
                        }, animationDelay);
                    }
                });
            }

            $window.on('scroll resize', checkIfInView);
            $window.trigger('scroll');
        }

        var animateEl = $('.is-animate');
		
		if ( $window.innerWidth() > 767 ) {
			animateElInit(animateEl);
		}
    }
};

export default AnimateOnScroll;