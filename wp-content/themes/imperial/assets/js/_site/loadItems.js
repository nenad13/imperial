const $ = jQuery.noConflict();

'use strict';
const LoadItems = {
	/*-------------------------------------------------------------------------------
		# Initialize
	-------------------------------------------------------------------------------*/
	init: function () {
		var referenceItems = '.js-reference-item';
		var workItems = '.js-works-item';
        var loadBtn = $('.js-load-items');

		function load(items, count) {
			$(items).hide();

			var itemsLength = $(items).length; //75
			
			$(items + ':lt(' + count + ')').show();
			if ($(window).width() < 768) {
				loadBtn.click(function () {
					count = ( count + 5 <= itemsLength) ? count + 5 : itemsLength;
					$(items + ':lt(' + count + ')').fadeIn();
	
					if ( count >= itemsLength ) {
						loadBtn.hide();
					}
				});
			}else {
				loadBtn.click(function () {
					count = ( count + 10 <= itemsLength) ? count + 10 : itemsLength;
					$(items + ':lt(' + count + ')').fadeIn();
	
					if ( count >= itemsLength ) {
						loadBtn.hide();
					}
				});
			}
			

			if ( count >= itemsLength ) {
				loadBtn.hide();
			}
		};

		if ($(window).width() < 768) {
			if ( $(workItems).length ) {
				load(workItems, 3);
			}
		}else {
			if ( $(workItems).length ) {
				load(workItems, 9);
			}
		}
		if ($(window).width() < 768) {
			if ( $(referenceItems).length ) {
				load(referenceItems, 5);
			}
		}else {
			if ( $(referenceItems).length ) {
				load(referenceItems, 10);
			}
		}
		
	}
};

export default LoadItems;