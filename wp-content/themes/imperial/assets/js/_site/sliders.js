const $ = jQuery.noConflict();

'use strict';
const Sliders = {

	/*-------------------------------------------------------------------------------
		# Initialize
	-------------------------------------------------------------------------------*/
	init: function () {
		var bannerSlider = $('.js-banner-slider');
		var workSlider = $('.js-slider');
		// bannerSlider
		bannerSlider.slick({
			slidesToShow: 1,
			lazyLoad: 'ondemand',
			arrows: false,
			fade: true,
			infinite: true,
			autoplay: true,
			autoplaySpeed: 3900,
			speed: 2500,
			pauseOnFocus: false,
			pauseOnHover: false
		});
		
		bannerSlider.find('.slick-current').addClass('active');    
		bannerSlider.on('beforeChange', function(event, slick, currentSlide, nextSlide){
			bannerSlider.find('.slick-slide').removeClass('progress active');
			bannerSlider.find('.slick-slide').eq(nextSlide).addClass('progress');        
		});

		// work slider
		workSlider.slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			mobileFirst: true,
			dots: true,
			arrows: false,
			responsive: [
				{
					breakpoint: 768,
					settings: 'unslick'
				}
			]
		});
	}
};

export default Sliders;
