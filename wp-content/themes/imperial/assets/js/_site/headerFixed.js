const $ = jQuery.noConflict();

'use strict';
const HeaderFixed = {
    /*-------------------------------------------------------------------------------
    	# Initialize
    -------------------------------------------------------------------------------*/
    init: function() {
		var $window = $(window);
		var header = $('.header');
        var classFixed = 'header-fixed';

        // if ( $window.width() > 1024 ) {
            $window.on('scroll', function() {
                if ( $window.scrollTop() > 100 ) {
                    header.addClass(classFixed);
                } else {
                    header.removeClass(classFixed);
                }
            });
        // }
    }
};

export default HeaderFixed;