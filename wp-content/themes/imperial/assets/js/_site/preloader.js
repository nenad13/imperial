const $ = jQuery.noConflict();

'use strict';
const Preloader = {
	/*-------------------------------------------------------------------------------
		# Initialize
	-------------------------------------------------------------------------------*/
	init: function () {
		var preloaderSel = $('.preloader');
		
		function preloader() {
			preloaderSel.addClass('loaded');
		}

		setTimeout(function() {
			preloader();
		}, 600);
	}
};

export default Preloader;