<div class="img-text">
    <div class="wrapper wrapper--sm">
        <div class="img-text__container">
            <div class="img-text__item is-animate slide-fade">
                <img src="<?php echo get_sub_field('image')['sizes']['reference']; ?>">
            </div>

            <div class="img-text__item is-animate slide-fade" data-slide-delay="600">
                <div class="img-text__item-content">
                    <div class="entry-content">
                        <?php echo get_sub_field('text'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>