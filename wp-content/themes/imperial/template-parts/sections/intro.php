<div class="intro">
	<div class="wrapper wrapper--sm">
		<div class="intro__container ">
			<?php if ( have_rows('box') ) : ?>
				<?php while ( have_rows('box') ) : the_row(); ?>

					<div class="intro__box is-animate slide-fade">
						<span class="intro__box-number"><?php echo get_sub_field('number'); ?></span>
						<span class="intro__box-number-text"><?php echo get_sub_field('text'); ?></span>
					</div>

				<?php endwhile; ?>
			<?php endif; ?>
			
			<span class="intro__box-text is-animate slide-fade" data-slide-delay="500"><?php echo get_sub_field('title'); ?></span>
			<div class="intro__box-content is-animate slide-fade" data-slide-delay="800">
				<div class="entry-content">
					<?php echo get_sub_field('text'); ?>
				</div>
				<div class="intro__box-content-btn is-animate slide-fade" data-slide-delay="900">
					<?php if ( have_rows('btn') ) : ?>
						<?php while ( have_rows('btn') ) : the_row(); ?>

							<a class="btn btn--primary" href="<?php echo get_sub_field('link'); ?>"><?php echo get_sub_field('label'); ?></a>

						<?php endwhile; ?>
					<?php endif; ?>
				</div>
			</div>	
		</div>
	</div>
</div>