<div class="img-slider img-slider--sm">
    <div class="img-slider__container">
    <?php if ( have_rows('imgs_items') ) : ?>
        <?php while ( have_rows('imgs_items') ) : the_row(); ?>
			<div class="img-slider__image js-carousel-item is-animate slide-fade">
				<div class="img-slider__image-wrap">
					<?php if ( have_rows('items') ) : ?>
						<?php while ( have_rows('items') ) : the_row(); ?>

						<img class="js-carousel-img" src="<?php echo get_sub_field('image')['sizes']['carusel']; ?>" alt="">

						<?php endwhile; ?>
					<?php endif; ?>
				</div>
			</div>
        <?php endwhile; ?>
    <?php endif; ?>
    </div>
</div>