<div class="contact-info">
	<div class="contact-info__container">
		<div class="contact-info__box">
			<?php if ( have_rows('contact_items') ) : ?>
				<?php while ( have_rows('contact_items') ) : the_row(); ?>

				<div class="contact-info__item">
					<span class="contact-info__title"><?php echo get_sub_field('title'); ?></span>
					<div class="contact-info__subtext"><?php echo get_sub_field('text'); ?></div>
				</div>

				<?php endwhile; ?>
			<?php endif; ?>
		</div>
		<div class="contact-info__image">
			<?php 
				$location = get_sub_field('map');
				if( $location ): ?>
					<div class="acf-map" data-zoom="16">
						<div class="marker" data-lat="<?php echo esc_attr($location['lat']); ?>" data-lng="<?php echo esc_attr($location['lng']); ?>"></div>
					</div>
			<?php endif; ?>
		</div>

	</div>
	<div class="contact-info__bottom">
		<div class="wrapper wrapper--sm">
			<div class="contact-info__bottom-container">
			<?php if ( have_rows('bottom_items') ) : ?>
				<?php while ( have_rows('bottom_items') ) : the_row(); ?>
					<div class="contact-info__bottom-item">
						<span class="contact-info__title"><?php echo get_sub_field('title'); ?><span class="contact-info__title-dots"> :</span></span>
						<span class="contact-info__subtext"><?php echo get_sub_field('number'); ?></span>
					</div>
				<?php endwhile; ?>
			<?php endif; ?>
			</div>
		</div>
	</div>
</div>
			

<div class="contact" id="contact-form">
	<div class="contact__text-box">
		<div class="wrapper wrapper--sm">
			<div class="contact__text-box-wrap">
				<h2 class="contact__text-box-title"><?php echo get_sub_field('title'); ?></h2>
				<h3 class="contact__text-box-subtitle"><?php echo get_sub_field('subtitle'); ?></h3>
			</div>
		</div>
	</div>
	<div class="wrapper wrapper--sm">
		<?php echo do_shortcode('[contact-form-7 id="182" title="Contact"]'); ?>
	</div>
</div>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCgmoptX14xDsJZ6zs59W-x5tlcvDGrl_E"></script>