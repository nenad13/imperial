<div class="certificates">
    <div class="wrapper wrapper--sm">
        <?php $title = get_sub_field('title'); ?>
        <?php if ( $title ) : ?>
            <div class="section-head">
                <h2 class="section-head__title section-head__title--left"><?php echo $title; ?></h2>
            </div>
         <?php endif; ?>
        <div class="certificates__container">
        <?php if ( have_rows('certificates_items') ) : ?>
            <?php while ( have_rows('certificates_items') ) : the_row(); ?>

                <div class="sertificates__item">
                    <div class="sertificates__item-img">
                        <img src="<?php echo get_sub_field('image')['url']; ?>" alt="">
                    </div>
                    <div class="sertificates__item-content">
                        
                    <?php $title = get_sub_field('title'); ?>
                        <?php if ( $title ) : ?>
                            <h4 class="sertificates__item-content-title"><?php echo $title; ?></h4>
                        <?php endif; ?>
                    </div>

                    <div class="sertificates__item-content-btn">
                    <?php if ( have_rows('certificates_btn') ) : ?>
                        <?php while ( have_rows('certificates_btn') ) : the_row(); ?>
                            <?php if( get_sub_field('file') ): ?>
                                <a href="<?php the_sub_field('file'); ?>" target="_blank"><?php echo get_sub_field('label'); ?></a>
                            <?php endif; ?>
                        <?php endwhile; ?>
                    <?php endif; ?>
                    </div>
                </div>
            <?php endwhile; ?>
        <?php endif; ?>
        </div>
    </div>
</div>