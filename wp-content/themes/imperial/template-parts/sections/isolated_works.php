
<?php $img_block_h = get_sub_field('img_block_height'); ?>

<?php 
    
    if ( $img_block_h == 'small' ) :
		$ib = 'img-block--sm';
	else :
        $ib = '';
	endif;
?>

<div class="img-block <?php echo $ib; ?>">
	<div class="wrapper wrapper--sm">
		<div class="section-head">
			<h2 class="section-head__title is-animate slide-fade"><?php echo get_sub_field('title'); ?></h2>
			<h3 class="section-head__subtitle is-animate slide-fade" data-slide-delay="300"><?php echo get_sub_field('subtitle'); ?></h3>
		</div>
	</div>
	<div class="img-block__container js-slider">
		
		<?php
			$args = array(
				'post_type' => 'work',
				'posts_per_page' => 5,
			);

			$query = new WP_Query( $args );
			?>

			<?php if ( $query->have_posts() ) : ?>
				<?php while ( $query->have_posts() ) : $query->the_post(); ?>
					<div class="img-block__image-wrap">
						<div class="img-block__image is-animate slide-fade" data-slide-delay="500">
							<a href="<?php the_permalink(); ?>">
								<img src="<?php echo get_the_post_thumbnail_url(null, 'rect'); ?>" alt="">
							</a>
							<div class="img-block__image-text">
								<a href="<?php the_permalink(); ?>">
									<span class="works__year"><?php echo get_field('year', $post_id); ?></span>
									<h3 class="works__name"><?php the_title(); ?></h3>
								</a>
							</div>
						</div>
					</div>
				<?php endwhile; ?>
			<?php endif; ?>

			<?php wp_reset_query(); ?>
		</div>
	</div>
	<div class="img-block__btn is-animate slide-fade" data-slide-delay="500">
		<?php if ( have_rows('works_btn') ) : ?>
			<?php while ( have_rows('works_btn') ) : the_row(); ?>
				<a class="btn btn--primary" href="<?php echo get_sub_field('link'); ?>"><?php echo get_sub_field('label'); ?></a>
			<?php endwhile; ?>
		<?php endif; ?>
	</div>
	<div class="certificates">
		<div class="wrapper wrapper--sm">
			<div class="section-head">
				<h2 class="section-head__title">Sertifikati</h2>
				<h3 class="section-head__subtitle">Sertifikati garantuju kvalitet naših radova</h3>
			</div>
			<div class="certificates__container">
				<div class="certificate-item">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/demo/certificate.png" alt="">
					<div class="certificates-item__text">
						<p>ISO 45001:2018</p>
					</div>
				</div>
				<div class="certificate-item">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/demo/certificate.png" alt="">
					<div class="certificates-item__text">
						<p>EN ISO 9001:2015</p>
					</div>
				</div>
				<div class="certificate-item">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/demo/certificate.png" alt="">
					<div class="certificates-item__text">
						<p>EN ISO 140001:2015</p>
					</div>
				</div>
				<div class="certificate-item">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/demo/certificate.png" alt="">
					<div class="certificates-item__text">
						<p>EN ISO 50001:2018</p>
					</div>
				</div>
				<div class="certificate-item">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/demo/certificate-2.png" alt="">
				</div>
				<div class="certificate-item">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/demo/certificate-3.png" alt="">
				</div>
			</div>
		</div>

	</div>
	<?php $bottom_text = get_sub_field('bottom_text'); ?>
	<?php if ($bottom_text) : ?>
		<span class="img-block__text is-animate slide-fade" data-slide-delay="600"><?php echo get_sub_field('bottom_text'); ?></span>
	<?php endif; ?>
</div>