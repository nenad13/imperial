<?php if ( have_rows('isolated_reference', 'option') ) : ?>
    <?php while ( have_rows('isolated_reference', 'option') ) : the_row(); ?>

		<div class="reference">
			<div class="wrapper wrapper--sm">
				<div class="reference__wrap">
					<div class="reference__image is-animate slide-fade">
						<img src="<?php echo get_sub_field('image', 'option')['sizes']['reference']; ?>">
					</div>
					<div class="reference__content">
						<div class="section-head">
							<h2 class="section-head__title section-head__title--left is-animate slide-fade"><?php echo get_sub_field('title', 'option'); ?></h2>
						</div>
						<div class="reference__content-wrap">
							<?php if ( have_rows('reference_items', 'option') ) : ?>
								<?php while ( have_rows('reference_items', 'option') ) : the_row(); ?>
									<div class="reference__content-item is-animate slide-fade" data-slide-delay="500">
										<h3 class="reference__content-title "><?php echo get_sub_field('firm', 'option'); ?></h3>
										<span class="reference__content-subtext"><?php echo get_sub_field('work', 'option'); ?></span>
										<span class="reference__content-place"><?php echo get_sub_field('place', 'option'); ?></span>
									</div>
								<?php endwhile; ?>
							<?php endif; ?>
							<div class="reference__content-btn is-animate slide-fade" data-slide-delay="400">
								<?php if ( have_rows('btn', 'option') ) : ?>
									<?php while ( have_rows('btn', 'option') ) : the_row(); ?>
										<a class="btn btn--primary" href="<?php echo get_sub_field('link'); ?>"><?php echo get_sub_field('label', 'option'); ?></a>
									<?php endwhile; ?>
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>


    <?php endwhile; ?>
<?php endif; ?>