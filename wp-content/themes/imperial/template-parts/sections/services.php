<?php $services = get_sub_field('services_height'); ?>

<?php 

    if ( $services == 'large' ) :
		$service_h = '';
	else :
        $service_h = 'services--sm';
    endif;
	
?>


<div id="primary" class="content-area">
		<main id="main" class="site-main">
            <div class="services <?php echo $service_h; ?>">
                <div class="wrapper wrapper--sm">
                    <?php $title = get_sub_field('title'); ?>
                    <?php if ($title) : ?>
                        <div class="section-head">
                            <h2 class="section-head__title is-animate slide-fade"><?php echo get_sub_field('title'); ?></h2>
                            <h3 class="section-head__subtitle is-animate slide-fade"data-slide-delay="500"><?php echo get_sub_field('subtitle'); ?></h3>
                        </div>
                    <?php endif; ?>
                    <div class="services__container">
                        <?php if ( have_rows('services_items') ) : ?>
                            <?php while ( have_rows('services_items') ) : the_row(); ?>
                                <div class="service-item is-animate slide-fade">
                                    <div class="service-item__wrap">
                                    <?php $image = get_sub_field('image'); ?>
                                    <?php if ($image) : ?>
                                        <div class="service-item__img">
                                            <img src="<?php echo get_sub_field('image')['sizes']['rect-sm']; ?>" alt="">
                                        </div>
                                    <?php endif; ?>
                                        <div class="service-item__content">
                                            <h3 class="service-item__content-title"><?php echo get_sub_field('title'); ?></h3>
                                            <div class="service-item__content-text">
                                                <div class="entry-content">
                                                   <p><?php echo get_sub_field('content'); ?></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endwhile; ?>
                        <?php endif; ?>
                    </div>
					<?php if ( have_rows('btn') ) : ?>
                        <?php while ( have_rows('btn') ) : the_row(); ?>
                    		<?php $label = get_sub_field('label'); ?>
                   			 <?php if ($label) : ?>
                        		<div class="services-btn is-animate slide-fade"data-slide-delay="1000">
                                    <a class="btn btn--primary" href="<?php echo get_sub_field('link'); ?>"><?php echo $label; ?></a>
                        		</div>
                    		<?php endif; ?>
						<?php endwhile; ?>
                    <?php endif; ?>
                </div>
            </div>
        </main><!-- #main -->
    </div><!-- #primary -->