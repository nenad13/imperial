<div class="reference reference--sm">
    <div class="wrapper wrapper--sm">
        <div class="section-head">
            <h2 class="section-head__title is-animate slide-fade"><?php echo get_sub_field('title'); ?></h2>
            <h3 class="section-head__subtitle is-animate slide-fade" data-slide-delay="300"><?php echo get_sub_field('subtitle'); ?></h3>
        </div>
        <div class="reference__container">
			<?php if ( have_rows('reference_items') ) : ?>
				<?php while ( have_rows('reference_items') ) : the_row(); ?>
					<div class="reference__content-box js-reference-item is-animate slide-fade">
						<h3 class="reference__content-title"><?php echo get_sub_field('firm'); ?></h3>
						<span class="reference__content-subtext"><?php echo get_sub_field('work'); ?></span>
						<span class="reference__content-place"><?php echo get_sub_field('place'); ?></span>
					</div>
				<?php endwhile; ?>
			<?php endif; ?>
        </div>
		<div class="reference__btn">
			<a class="btn btn--primary js-load-items" href="javascript:;"><?php _e('Učitaj još', 'imperial'); ?></a>
		</div>
    </div>
</div>