<?php $bg_color = get_sub_field('background_color'); ?>
<?php $bg_img = get_sub_field('background_image'); ?>
<?php $subtext = get_sub_field('subtext'); ?>
<?php $text_block_wrap = get_sub_field('text_block_width'); ?>
<?php $text_block_h = get_sub_field('text_block_height'); ?>
<?php $text_color = get_sub_field('text_color'); ?>

<?php 
	if (!$bg_img) :
		if ( $bg_color == 'red' ) :
			$bg = 'text-block--red';
			$border = 'btn--primary-white';
		elseif ( $bg_color == 'white') :
			$bg = 'text-block--white';
			$border = '';
		else :
			$bg = '';
			$border = '';
		endif;
	else :
		$border = 'btn--red';
	endif;
    
    if ( $text_block_wrap == 'large' ) :
        $tbw = 'text-block__wrap--lg ';
	else :
        $tbw = '';
    endif;

    if ( $text_block_h == 'large' ) :
		$tb = 'text-block--lg';
	else :
        $tb = '';
	endif;
	if ( $text_block_h == 'xssmall' ) :
		$tb = 'text-block--xs';
	endif;
	
	 if ( $text_color == 'light' ) :
        $color = 'text-block--light';
		$ec = 'entry-content--light';
	else :
        $color = '';
		$ec = '';
    endif;
?>

<div class="text-block <?php echo $bg; ?> <?php echo $tb; ?> <?php echo $color; ?>"style="background-image: url(<?php echo $bg_img['url']; ?>">
    <div class="wrapper wrapper--sm">
        <div class="text-block__wrap <?php echo $tbw; ?>">
			<?php $title = get_sub_field('title'); ?>
			<?php if ( $title ) : ?>
				<h3 class="text-block__title is-animate slide-fade"><?php echo $title; ?></h3>
			<?php endif; ?>
			<?php if ( $subtext ) : ?>
				<p class="text-block__txt-strong is-animate slide-fade"><?php echo $subtext; ?></p>
			<?php endif; ?>
            <div class="text-block__txt is-animate slide-fade">
                <div class="entry-content <?php echo $ec; ?>">
                    <?php echo get_sub_field('text'); ?>
                </div>
            </div>
            <div class="text-block__btn is-animate slide-fade" data-slide-delay="700">
            <?php if ( have_rows('btn') ) : ?>
                <?php while ( have_rows('btn') ) : the_row(); ?>
				<?php $label = get_sub_field('label'); ?>
				<?php if ($label) : ?>
					<a class="btn btn--primary <?php echo $border ?>" href="<?php echo get_sub_field('link'); ?>"><?php echo $label; ?></a>
				<?php endif; ?>
                <?php endwhile; ?>
            <?php endif; ?>
            </div>
        </div>
		
		<?php $image = get_sub_field('image'); ?>
		<?php if ($image) : ?>
			<div class="text-block__img">
				<img src="<?php echo $image['sizes']['rect-lg'] ?>" alt="">
			</div>
		<?php endif; ?>
    </div>
</div>