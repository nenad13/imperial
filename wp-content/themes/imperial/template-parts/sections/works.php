<div class="works">
	<div class="wrapper wrapper--sm">
		<div class="works__container">
		
		<?php
			$args = array(
				'post_type' => 'work',
				'posts_per_page' => -1,
			);

			$query = new WP_Query( $args );
			?>

			<?php if ( $query->have_posts() ) : ?>
				<?php while ( $query->have_posts() ) : $query->the_post(); ?>
					
					<div class="works__item js-works-item ">
						<a href="<?php the_permalink(); ?>">
							<div class="works__item-img">
								<img src="<?php echo get_the_post_thumbnail_url(null, 'rect'); ?>" alt="">
							</div>
							<div class="works__item-content">
								<span class="works__item-year"><?php echo get_field('year'); ?>.</span>
								<span class="works__item-name"><?php the_title(); ?></span>
							</div>
						</a>
					</div>
				<?php endwhile; ?>
			<?php endif; ?>

			<?php wp_reset_query(); ?>
		</div>

		<div class="works__btn">
			<a class="btn btn--primary js-load-items" href="javascript:;"><?php _e('Učitaj još', 'imperial'); ?></a>
		</div>
	</div>
</div>