<?php if ( is_front_page() ) : ?>
	<?php $banner_sm = ''; ?>
<?php else : ?>
	<?php $banner_sm = 'banner__img--sm'; ?>
<?php endif; ?>

<?php if (count(get_field('banner_images')) > 1) : ?>
	<?php $banner_progress = 'banner__img--progress'; ?>
<?php else : ?>
	<?php $banner_progress = ''; ?>
<?php endif; ?>
	
<div class="banner">
	<div class="banner__bg js-banner-slider">
		<?php if ( have_rows('banner_images') ) : ?>
			<?php while ( have_rows('banner_images') ) : the_row(); ?>
				<div class="banner__img <?php echo $banner_sm . ' ' . $banner_progress; ?>" style="background-image: url(<?php echo get_sub_field('banner_image')['url']; ?>"></div>
			<?php endwhile; ?>
		<?php endif; ?>
	</div>
	<div class="banner__top">
		<div class="wrapper wrapper--sm">
			<div class="banner__content">
				<h1 class="banner__content-title is-animate slide-fade"><div class="underline"><?php echo get_field('banner_title'); ?></div></h1>
				<?php $text = get_field('banner_text'); ?>
				<?php if ($text) : ?>
					<div class="banner__content-text is-animate slide-fade" data-slide-delay="400">
						<p><?php echo $text; ?></p>
					</div>
				<?php endif; ?>
				
				<div class="banner__content-btns is-animate slide-fade" data-slide-delay="800">
					<?php if ( have_rows('banner_btn_1') ) : ?>
						<?php while ( have_rows('banner_btn_1') ) : the_row(); ?>
							<?php $label = get_sub_field('label'); ?>
							<?php if ($label) : ?>
								<a class="btn btn--red" href="<?php echo get_sub_field('link'); ?>"><?php echo $label; ?></a>
							<?php endif; ?>

						<?php endwhile; ?>
					<?php endif; ?>
					<?php if ( have_rows('banner_btn_2') ) : ?>
						<?php while ( have_rows('banner_btn_2') ) : the_row(); ?>
							<?php $label = get_sub_field('label'); ?>
							<?php if ($label) : ?>
								<a class="btn btn--ghost" href="<?php echo get_sub_field('link'); ?>"><?php echo $label; ?></a>
							<?php endif; ?>
						<?php endwhile; ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</div>