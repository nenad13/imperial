<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package imperial
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function imperial_body_classes( $classes ) {
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	return $classes;
}
add_filter( 'body_class', 'imperial_body_classes' );


/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function imperial_pingback_header() {
	if ( is_singular() && pings_open() ) {
		echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';
	}
}
add_action( 'wp_head', 'imperial_pingback_header' );


/**
 * Modify WP Login
 */
// change error message
function imperial_error_message() {
	return 'Wrong username or password.';
}
add_filter( 'login_errors', 'imperial_error_message' );

// remove error shaking
function imperial_remove_login_shake() {
	remove_action( 'login_head', 'wp_shake_js', 12 );
}
add_action( 'login_head', 'imperial_remove_login_shake' );

// add custom stylesheet
function imperial_add_login_styles() {
	wp_enqueue_style('imperial-login-style', get_template_directory_uri() . '/assets/config/customize-login/login.css' );
}
add_action( 'login_enqueue_scripts', 'imperial_add_login_styles' );

// add login title
function imperial_add_login_title() {
	echo '<span class="login-title">imperial login</span>';
}
add_action( 'login_form', 'imperial_add_login_title' );

// change logo url
function imperial_loginlogo_url($url) {
	$url = esc_url( home_url( '/' ) );
	return $url;
}
add_filter( 'login_headerurl', 'imperial_loginlogo_url' );


/**
 * Plugin dependencies
 */
function imperial_dependencies() {
	if( ! function_exists('get_field') ) {
		echo '<div class="error"><p>' . __( 'Warning: The theme needs ACF Pro plugin to function', 'imperial' ) . '</p></div>';
	}
}
add_action( 'admin_notices', 'imperial_dependencies' );

/**
 * Allow SVG through Wordpress Media Uploader
 */
add_filter( 'wp_check_filetype_and_ext', function($data, $file, $filename, $mimes) {

	global $wp_version;
	
	$filetype = wp_check_filetype( $filename, $mimes );
	
	return [ 'ext' => $filetype['ext'], 'type' => $filetype['type'], 'proper_filename' => $data['proper_filename'] ];
	
}, 10, 4 );
	
function cc_mime_types( $mimes ){ $mimes['svg'] = 'image/svg+xml'; return $mimes; } add_filter( 'upload_mimes', 'cc_mime_types' );