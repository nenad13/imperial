<?php
/**
 * Template name: Sertifikati
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package imperial
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<div class="banner banner--sm"style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/images/demo/reference.png)">
				<div class="banner__top">
					<div class="wrapper wrapper--sm">
						<div class="banner__content">
							<h1 class="banner__content-title is-animate slide-fade"><span class="underline"> Mi gradimo,</span></br>mi menjamo budućnost</h1>
						</div>
					</div>
				</div>
			</div>

			<div class="certificates">
                <div class="wrapper wrapper--sm">
                    <div class="section-head">
                        <h2 class="section-head__title section-head__title--left">Za preuzimanje fajlova kliknite na link za preuzimanje</h2>
                    </div>
                    <div class="certificates__container">
                        <div class="sertificates__item">
                            <div class="sertificates__item-img">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/download.png" alt="">
                            </div>
                            <div class="sertificates__item-content">
                                <h4 class="sertificates__item-content-title">Politika integrisanog sistema menadžmenta</h4>
                            </div>
                            <div class="sertificates__item-content-btn">
                                <a target="_blank" href="http://localhost/imperial/wp-content/uploads/2020/05/politika-integrisanog-sistema-menadzmenta.pdf">Preuzmi</a>
                            </div>
                        </div>
                        <div class="sertificates__item">
                            <div class="sertificates__item-img">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/download.png" alt="">
                            </div>
                            <div class="sertificates__item-content">
                                <h4 class="sertificates__item-content-title">ISO 9001-2015-2020</h4>
                            </div>
                            <div class="sertificates__item-content-btn">
                                <a href="http://localhost/imperial/wp-content/uploads/2020/05/ISO-9001-2015-2020.jpg"target="_blank">Preuzmi</a>
                            </div>
                        </div>
                        <div class="sertificates__item">
                            <div class="sertificates__item-img">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/download.png" alt="">
                            </div>
                            <div class="sertificates__item-content">
                                <h4 class="sertificates__item-content-title">Sertifikat 9001</h4>
                            </div>
                            <div class="sertificates__item-content-btn">
                                <a href="http://localhost/imperial/wp-content/uploads/2020/05/9001.jpg"target="_blank">Preuzmi</a>
                            </div>
                        </div>
                        <div class="sertificates__item">
                            <div class="sertificates__item-img">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/download.png" alt="">
                            </div>
                            <div class="sertificates__item-content">
                                <h4 class="sertificates__item-content-title">Sertifikat 14001</h4>
                            </div>
                            <div class="sertificates__item-content-btn">
                                <a href="http://localhost/imperial/wp-content/uploads/2020/05/14001.jpg"target="_blank">Preuzmi</a>
                            </div>
                        </div>
                        <div class="sertificates__item">
                            <div class="sertificates__item-img">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/download.png" alt="">
                            </div>
                            <div class="sertificates__item-content">
                                <h4 class="sertificates__item-content-title">Sertifikat 18001</h4>
                            </div>
                            <div class="sertificates__item-content-btn">
                                <a href="http://localhost/imperial/wp-content/uploads/2020/05/18001.jpg"target="_blank">Preuzmi</a>
                            </div>
                        </div>
                        <div class="sertificates__item">
                            <div class="sertificates__item-img">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/download.png" alt="">
                            </div>
                            <div class="sertificates__item-content">
                                <h4 class="sertificates__item-content-title">ISO 14001-2015-2020</h4>
                            </div>
                            <div class="sertificates__item-content-btn">
                                <a href="http://localhost/imperial/wp-content/uploads/2020/05/ISO-14001-2015-2020.jpg"target="_blank">Preuzmi</a>
                            </div>
                        </div>
                        <div class="sertificates__item">
                            <div class="sertificates__item-img">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/download.png" alt="">
                            </div>
                            <div class="sertificates__item-content">
                                <h4 class="sertificates__item-content-title">ISO 9001-2015-2020</h4>
                            </div>
                            <div class="sertificates__item-content-btn">
                                <a href="http://localhost/imperial/wp-content/uploads/2020/05/ISO-9001-2015-2020.jpg"target="_blank">Preuzmi</a>
                            </div>
                        </div>
                        <div class="sertificates__item">
                            <div class="sertificates__item-img">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/download.png" alt="">
                            </div>
                            <div class="sertificates__item-content">
                                <h4 class="sertificates__item-content-title">Sertifikat</br> OHSAS-TMS-2018-2020</h4>
                            </div>
                            <div class="sertificates__item-content-btn">
                                <a href="http://localhost/imperial/wp-content/uploads/2020/05/OHSAS-TMS-2018-2020.jpg" target="_blank">Preuzmi</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			

            <div class="img-block img-block--sm">
				<div class="img-block__container">
					<div class="img-block__image">
						<img class="is-animate slide-fade" src="<?php echo get_template_directory_uri(); ?>/assets/images/demo/1.png" alt="">
					</div>
					<div class="img-block__image">
						<img class="is-animate slide-fade" data-slide-delay="400" src="<?php echo get_template_directory_uri(); ?>/assets/images/demo/2.png" alt="">
					</div>
					<div class="img-block__image">
						<img class="is-animate slide-fade" data-slide-delay="600" src="<?php echo get_template_directory_uri(); ?>/assets/images/demo/3.png" alt="">
					</div>
					<div class="img-block__image">
						<img class="is-animate slide-fade" data-slide-delay="800" src="<?php echo get_template_directory_uri(); ?>/assets/images/demo/4.png" alt="">
					</div>
					<div class="img-block__image">
						<img class="is-animate slide-fade" data-slide-delay="1000" src="<?php echo get_template_directory_uri(); ?>/assets/images/demo/1.png" alt="">
					</div>
				</div>
			</div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
