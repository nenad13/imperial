<?php
/**
 * Template name: Usluge
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package imperial
 */

get_header(); ?>
    <div id="primary" class="content-area">
		<main id="main" class="site-main">
            <div class="services">
                <div class="wrapper wrapper--sm">
                    <div class="section-head">
                        <h2 class="section-head__title">Usluge</h2>
                        <h3 class="section-head__subtitle">Izvodimo radove na infrastrukturnom opremanju:</h3>
                    </div>
                    <div class="services__container">
                        <div class="service-item">
                            <div class="service-item__wrap">
                                <div class="service-item__img">
                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/demo/44.png" alt="">
                                </div>
                                <div class="service-item__content">
                                    <h3 class="service-item__content-title">Građevinski radovi</h3>
                                    <div class="service-item__content-text">
                                        <div class="entry-content">
                                            <p>Građevinski radovi na izgradnji kanalizacione i vodovodne mreže, vrelovoda, gasovoda, putne infrastrukture, kao i ostale građevinske radove.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="service-item">
                            <div class="service-item__wrap">
                                <div class="service-item__img">
                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/demo/44.png" alt="">
                                </div>
                                <div class="service-item__content">
                                    <h3 class="service-item__content-title">Mašinski radovi</h3>
                                    <div class="service-item__content-text">
                                        <div class="entry-content">
                                            <p>Građevinski radovi na izgradnji kanalizacione i vodovodne mreže, vrelovoda, gasovoda, putne infrastrukture, kao i ostale građevinske radove.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main><!-- #main -->
    </div><!-- #primary -->

<?php
get_footer();