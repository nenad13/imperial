<?php
/**
 * Template name: Radovi
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package imperial
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<div class="banner banner--sm"style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/images/demo/reference.png)">
				<div class="banner__top">
					<div class="wrapper wrapper--sm">
						<div class="banner__content">
							<h1 class="banner__content-title is-animate slide-fade">Otkrijte <span class="underline">naše mogućnosti</span></h1>
						</div>
					</div>
				</div>
			</div>

			<div class="text-block text-block--sm text-block--white">
				<div class="wrapper wrapper--sm">
					<div class="text-block__wrap text-block__wrap--lg">
						<h4 class="text-block__title is-animate slide-fade">Snaga svakog preduzeća su njegovi zaposleni. Naša vizija poslovanja to smo svi mi, pojedinci sa svojom inicijativom, kolektiv sa svojom snagom. Verujemo da je zajednički uspeh moguć samo razvojem preduzimljivosti, entuzijazma i vere u uspeh svakog pojedinca.</h4>
						<div class="text-block__txt is-animate slide-fade">
							<div class="entry-content">
								<p>Ako želite da postanete deo našeg tima, da sa Vama budemo još snažniji, bolji i uspešniji, pridružite nam se. Pažljivo popunite upitnik, pošaljite Vašu biografiju i propratno pismo, a mi ćemo Vam se javiti.</p>
							</div>
						</div>
					</div>
				</div>
            </div>

            <div class="gallery">
                <div class="wrapper wrapper--sm">
                    <div class="gallery__container">
                        <div class="gallery__item">
                            <div class="galery__item-img">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/demo/gallery.png" alt="">
                            </div>
                            <div class="gallery__item-content">
                                <span class="gallery__item-year">Godina izvođenja radova: 2013.</span>
                                <span class="galery__item-name" > Novosadska toplana: Mišeluk</span>
                            </div>
                            <div class="gallery__item-btn">
                                <a class="btn btn--ghost" href="javascript:;">Pogledaj galeriju</a>
                            </div>
                        </div>

                        <div class="gallery__item">
                            <div class="galery__item-img">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/demo/gallery.png" alt="">
                            </div>
                            <div class="gallery__item-content">
                                <span class="gallery__item-year">Godina izvođenja radova: 2013.</span>
                                <span class="galery__item-name" >Imperial Buildings radovi</span>
                            </div>
                            <div class="gallery__item-btn">
                                <a class="btn btn--ghost" href="javascript:;">Pogledaj galeriju</a>
                            </div>
                        </div>

                        <div class="gallery__item">
                            <div class="galery__item-img">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/demo/gallery.png" alt="">
                            </div>
                            <div class="gallery__item-content">
                                <span class="gallery__item-year">Godina izvođenja radova: 2013.</span>
                                <span class="galery__item-name" >Srbijagas: Izgradnja turskog toka</span>
                            </div>
                            <div class="gallery__item-btn">
                                <a class="btn btn--ghost" href="javascript:;">Pogledaj galeriju</a>
                            </div>
                        </div>
                        <div class="gallery__item">
                            <div class="galery__item-img">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/demo/gallery.png" alt="">
                            </div>
                            <div class="gallery__item-content">
                                <span class="gallery__item-year">Godina izvođenja radova: 2013.</span>
                                <span class="galery__item-name" > Novosadska toplana: Mišeluk</span>
                            </div>
                            <div class="gallery__item-btn">
                                <a class="btn btn--ghost" href="javascript:;">Pogledaj galeriju</a>
                            </div>
                        </div>
                        <div class="gallery__item">
                            <div class="galery__item-img">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/demo/gallery.png" alt="">
                            </div>
                            <div class="gallery__item-content">
                                <span class="gallery__item-year">Godina izvođenja radova: 2013.</span>
                                <span class="galery__item-name" >Imperial Buildings radovi</span>
                            </div>
                            <div class="gallery__item-btn">
                                <a class="btn btn--ghost" href="javascript:;">Pogledaj galeriju</a>
                            </div>
                        </div>
                        <div class="gallery__item">
                            <div class="galery__item-img">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/demo/gallery.png" alt="">
                            </div>
                            <div class="gallery__item-content">
                                <span class="gallery__item-year">Godina izvođenja radova: 2013.</span>
                                <span class="galery__item-name" >Srbijagas: Izgradnja turskog toka</span>
                            </div>
                            <div class="gallery__item-btn">
                                <a class="btn btn--ghost" href="javascript:;">Pogledaj galeriju</a>
                            </div>
                        </div>
                        <div class="gallery__item">
                            <div class="galery__item-img">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/demo/gallery.png" alt="">
                            </div>
                            <div class="gallery__item-content">
                                <span class="gallery__item-year">Godina izvođenja radova: 2013.</span>
                                <span class="galery__item-name" >Novosadska toplana: Mišeluk</span>
                            </div>
                            <div class="gallery__item-btn">
                                <a class="btn btn--ghost" href="javascript:;">Pogledaj galeriju</a>
                            </div>
                        </div>
                        <div class="gallery__item">
                            <div class="galery__item-img">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/demo/gallery.png" alt="">
                            </div>
                            <div class="gallery__item-content">
                                <span class="gallery__item-year">Godina izvođenja radova: 2013.</span>
                                <span class="galery__item-name">Imperial Buildings radovi</span>
                            </div>
                            <div class="gallery__item-btn">
                                <a class="btn btn--ghost" href="javascript:;">Pogledaj galeriju</a>
                            </div>
                        </div>
                        <div class="gallery__item">
                            <div class="galery__item-img">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/demo/gallery.png" alt="">
                            </div>
                            <div class="gallery__item-content">
                                <span class="gallery__item-year">Godina izvođenja radova: 2013.</span>
                                <span class="galery__item-name">Srbijagas: Izgradnja turskog toka</span>
                            </div>
                            <div class="gallery__item-btn">
                                <a class="btn btn--ghost" href="javascript:;">Pogledaj galeriju</a>
                            </div>
                        </div>
                        <div class="gallery__btn">
                            <a class="btn btn--primary" href="javascript:;">Učitaj još</a>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="reference">
				<div class="wrapper wrapper--sm">
					<div class="reference__container">
						<div class="reference__slider js-reference-slider">
							<div class="reference__image is-animate slide-fade">
								<img src="<?php echo get_template_directory_uri(); ?>/assets/images/demo/1.png" alt="">
							</div>
							<div class="reference__image is-animate slide-fade">
								<img src="<?php echo get_template_directory_uri(); ?>/assets/images/demo/2.png" alt="">
							</div>
							<div class="reference__image is-animate slide-fade">
								<img src="<?php echo get_template_directory_uri(); ?>/assets/images/demo/3.png" alt="">
							</div>
						</div>
				
						<div class="reference__content">
							<div class="section-head">
								<h2 class="section-head__title section-head__title--left is-animate slide-fade">Reference</h2>
							</div>
							<div class="reference__content-wrap">
								<div class="reference__content-item is-animate slide-fade" data-slide-delay="500">
									<h3 class="reference__content-title ">JKP Vodovod i kanalizacija, Novi Sad</h3>
									<span class="reference__content-subtext">Rekonstrukcija vodovodne mreže</span>
									<span class="reference__content-place">Beogradski kej, Novi Sad</span>
									
								</div>
								<div class="reference__content-item is-animate slide-fade" data-slide-delay="700">
									<h3 class="reference__content-title">Aling-Conel d.o.o.</h3>
									<span class="reference__content-subtext ">Izvođenje radova na vrelovodnoj mreži</span>
									<span class="reference__content-place">Urgentni centar, Novi Sad</span>
								</div>
								<div class="reference__content-item is-animate slide-fade" data-slide-delay="900">
									<h3 class="reference__content-title">Millenium Team d.o.o.</h3>
									<span class="reference__content-subtext">Kanalizaciona mreža</span>
									<span class="reference__content-place">Žabalj</span>
								</div>
								<div class="reference__content-item is-animate slide-fade"data-slide-delay="1100">
									<h3 class="reference__content-title">Vojvodina put, Bačka put</h3>
									<span class="reference__content-subtext">Atmosferska kanalizacija</span>
									<span class="reference__content-place">Temerinska ulica, Novi Sad</span>
								</div>
								<div class="reference__content-btn is-animate slide-fade"data-slide-delay="1300">
									<a class="btn btn--primary" href="javascript:;">Vidi sve</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
            
            <div class="text-block text-block--red">
				<div class="wrapper wrapper--sm">
					<div class="text-block__wrap">
						<h4 class="text-block__title is-animate slide-fade">Ulažemo u svoju budućnost</h4>
						<div class="text-block__txt is-animate slide-fade" data-slide-delay="500">
							<div class="entry-content">
								<p>Ako želite da postanete deo našeg tima, da sa Vama budemo još snažniji, bolji i uspešniji, pridružite nam se</p>
							</div>
						</div>
						<div class="text-block__btn is-animate slide-fade" data-slide-delay="700">
							<a class="btn btn--primary btn--primary-white" href="javascript:;">Saznaj više</a>
						</div>
					</div>
				</div>
			</div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
