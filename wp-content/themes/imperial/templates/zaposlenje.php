<?php
/**
 * Template name: Zaposlenje
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package imperial
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<div class="banner banner--sm"style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/images/demo/reference.png)">
				<div class="banner__top">
					<div class="wrapper wrapper--sm">
						<div class="banner__content">
							<h1 class="banner__content-title is-animate slide-fade">Ulažemo u <span class="underline">svoju budućnost</span></h1>
						</div>
					</div>
				</div>
			</div>

			<div class="text-block text-block--sm text-block--white">
				<div class="wrapper wrapper--sm">
					<div class="text-block__wrap text-block__wrap--lg">
						<h4 class="text-block__title is-animate slide-fade">Snaga svakog preduzeća su njegovi zaposleni. Naša vizija poslovanja to smo svi mi, pojedinci sa svojom inicijativom, kolektiv sa svojom snagom. Verujemo da je zajednički uspeh moguć samo razvojem preduzimljivosti, entuzijazma i vere u uspeh svakog pojedinca.</h4>
						<div class="text-block__txt is-animate slide-fade" data-slide-delay="500">
							<div class="entry-content">
								<p>Ako želite da postanete deo našeg tima, da sa Vama budemo još snažniji, bolji i uspešniji, pridružite nam se. Pažljivo popunite upitnik, pošaljite Vašu biografiju i propratno pismo, a mi ćemo Vam se javiti.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="contact">
				<div class="wrapper wrapper--sm">
					<form class="contact__form">
						<div class="contact__item">
							<label class="sr-only" for="name">Ime i prezime</label>
							<input type="text" id="name" name="name" placeholder="Ime i prezime *">
						</div>
						<div class="contact__item">
							<label class="sr-only" for="gm">Godian i mesto rodjenja</label>
							<input type="text" id="gm" name="gm" placeholder="Godina i mesto rođenja *">
						</div>

						<div class="contact__item">
							<label class="sr-only" for="tel">Kontakt telefon</label>
							<input type="tel" id="tel" name="tel" placeholder="Kontakt telefon *">
						</div>

						<div class="contact__item">
							<label class="sr-only" for="e-mail">Email adresa</label>
							<input type="email" id="e-mail" name="mail" placeholder="Email adresa *">
						</div>

						<div class="contact__item">
							<label class="sr-only" for="oducation">Obrazovanje</label>
							<input type="text" id="education" name="education" placeholder="Stečeno obrazovanje *">
						</div>

						<div class="contact__item">
							<label class="custom-file-upload" for="bio">Dodajte svoju biografiju *</label>
							<input type="file" id="bio" name="bio" placeholder=" Dodajte svoju biografiju *">
						</div>

						<div class="contact__item-textarea">
							<label class="sr-only" for="letter">Propratno pismo  *</label>
							<textarea id="letter" name="letter" placeholder=" Propratno pismo *"></textarea>
						</div>
						<div class="contact__form-btn">
							<button class="btn btn--ghost">Pošalji</button>
						</div>
					</form>
				</div>
			</div>

            <div class="img-block img-block--sm">
				<div class="img-block__container">
					<div class="img-block__image">
						<img class="is-animate slide-fade" src="<?php echo get_template_directory_uri(); ?>/assets/images/demo/1.png" alt="">
					</div>
					<div class="img-block__image">
						<img class="is-animate slide-fade" data-slide-delay="400" src="<?php echo get_template_directory_uri(); ?>/assets/images/demo/2.png" alt="">
					</div>
					<div class="img-block__image">
						<img class="is-animate slide-fade" data-slide-delay="600" src="<?php echo get_template_directory_uri(); ?>/assets/images/demo/3.png" alt="">
					</div>
					<div class="img-block__image">
						<img class="is-animate slide-fade" data-slide-delay="800" src="<?php echo get_template_directory_uri(); ?>/assets/images/demo/4.png" alt="">
					</div>
					<div class="img-block__image">
						<img class="is-animate slide-fade" data-slide-delay="1000" src="<?php echo get_template_directory_uri(); ?>/assets/images/demo/1.png" alt="">
					</div>
				</div>
			</div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
