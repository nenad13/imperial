<?php
/**
 * Template name: About
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package imperial
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<div class="banner banner--sm"style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/images/demo/about.png)">
				<div class="banner__top">
					<div class="wrapper wrapper--sm">
						<div class="banner__content">
							<h1 class="banner__content-title is-animate slide-fade"><span class="underline">Mi gradimo,</span></br>mi menjamo budućnost</h1>
						</div>
					</div>
				</div>
			</div>

			<div class="text-block text-block--sm text-block--white">
				<div class="wrapper wrapper--sm">
					<div class="text-block__wrap">
						<h4 class="text-block__title is-animate slide-fade">Firma je osnovana 2007. godine u Novom Sadu kao društvo za inženjering, projektovanje, niskogradnju i visokogradnju.</h4>
						<div class="text-block__txt is-animate slide-fade" data-slide-delay="300">
							<div class="entry-content">
								<p>Naše preduzeće izvodi građevinske radove na izgradnji kanalizacione, vodovodne i gasne mreže zatim na izgradnji vrelovoda, kao i ostale građevinske radove.</p>
							</div>
						</div>
					</div>
				</div>
            </div>
            
            <div class="img-text">
                <div class="wrapper wrapper--sm">
                    <div class="img-text__container">
                        <div class="img-text__item is-animate slide-fade">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/demo/16.png" alt="">
                        </div>

                        <div class="img-text__item is-animate slide-fade" data-slide-delay="600">
                            <div class="img-text__item-content">
                                <div class="entry-content">
									<p>Sama organizacija u toku i za vreme izvođenja radova je u nadležnosti tehničke službe preduzeća, koja je u obavezi da pristupi organizaciji od početka izvođenja radova, pa do samog završetka radova. Do same predaje gradilišta tehnička služba je u saradnji sa nadzornim organima od strane investitora ili naručioca, kao i sa zaposlenim radnicima koji su angažovani na radovima.</p>

									<p> Posedujemo sopstvenu mehanizaciju, vozni park i tehničku opremljenost za obavljanje svih navedenih delatnosti. Preduzeće broji oko 40 zaposlenih, a uskladu sa potrebama angažuju se sezonski radnici. Kompanija ima sve službe potrebne za uspešnu realizaciju građevinske proizvodnje:</p>
									<ul>
										<li>Komercijalu</li>
										<li>Finansijsko/operativnu službu</li>
										<li>Projektovanje, inžinjering, izgradnju i mehanizaciju</li>
									</ul>
                                    <p>Generalni direktor kompanije je Ilija Mandić diplomirani ekonomista.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="img-block">
				<div class="wrapper wrapper--sm">
					<div class="section-head">
						<h2 class="section-head__title is-animate slide-fade">Izvedeni radovi</h2>
						<h3 class="section-head__subtitle is-animate slide-fade" data-slide-delay="500">Izvođenje radova je u skladu sa zaštitom životne sredine i ljudskog zdravlja.Prilagođavamo se željama i potrebama svih potencijalnih saradnika, poštujemo rokove i obećavamo dobar kvalitet gradnje.</h3>
					</div>
                </div>
                
				<div class="img-block__container">
					<div class="img-block__image is-animate slide-fade">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/demo/img1.png" alt="">
					</div>
					<div class="img-block__image is-animate slide-fade"data-slide-delay="500">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/demo/img1.png" alt="">
					</div>
					<div class="img-block__image is-animate slide-fade"data-slide-delay="700">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/demo/img1.png" alt="">
					</div>
					<div class="img-block__image is-animate slide-fade"data-slide-delay="900">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/demo/img1.png" alt="">
					</div>
					<div class="img-block__image is-animate slide-fade"data-slide-delay="1100">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/demo/img1.png" alt="">
					</div>
                </div>
                
				<div class="img-block__btn is-animate slide-fade"data-slide-delay="1300">
					<a class="btn btn--primary" href="javascript:;">Vidi galeriju</a>
                </div>
                
				<span class="img-block__text is-animate slide-fade" data-slide-delay="1500">U realizaciji Vaših projekata, odaberite Imperial Buildings</span>
            </div>
            
            <div class="text-block text-block--red">
				<div class="wrapper wrapper--sm">
					<div class="text-block__wrap">
						<h4 class="text-block__title is-animate slide-fade">Ulažemo u svoju budućnost</h4>
						<div class="text-block__txt is-animate slide-fade" data-slide-delay="500">
							<div class="entry-content">
								<p>Ako želite da postanete deo našeg tima, da sa Vama budemo još snažniji, bolji i uspešniji, pridružite nam se</p>
							</div>
						</div>
						<div class="text-block__btn is-animate slide-fade" data-slide-delay="700">
							<a class="btn btn--primary btn--primary-white" href="javascript:;">Saznaj više</a>
						</div>
					</div>
				</div>
			</div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
