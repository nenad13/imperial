<?php
/**
 * Template name: Reference
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package imperial
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<div class="banner banner--sm"style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/images/demo/reference.png)">
				<div class="banner__top">
					<div class="wrapper wrapper--sm">
						<div class="banner__content">
							<h1 class="banner__content-title is-animate slide-fade">Otkrijte <span class="underline">naše mogućnosti</span></h1>
						</div>
					</div>
				</div>
			</div>

			<div class="text-block text-block--sm text-block--white">
				<div class="wrapper wrapper--sm">
					<div class="text-block__wrap text-block__wrap--lg">
						<h4 class="text-block__title is-animate slide-fade"> Uspešno rešavamo sve zahteve investitora, ma koliko oni bili komplikovani. Vodimo računa da idemo u korak sa vremenom, pratimo i upotrebljavamo najsavremenije materijale i tehnologije kako u projektovanju, tako i u niskogradnji, visokogradnji i svim ostalim građevinskim radovima.</h4>
						<div class="text-block__txt is-animate slide-fade">
							<div class="entry-content">
								<p>Visoko profesionalni odnos zaposlenih, odanost i posvećenost prema radu, kvalitet usluga i stručnost kadra, poštovanje svih ugovorenih obaveza i rokova je razlog da svaki investitor ili naručioc posla bude siguran da ima pouzdanog i odgovornog izvođača.</p>
							</div>
						</div>
					</div>
				</div>
            </div>
            
            <div class="text-block" style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/images/demo/reference2.png)">
				<div class="wrapper wrapper--sm">
					<div class="text-block__wrap text-block__wrap--lg">
						<p class="text-block__txt-strong is-animate slide-fade">Pri projektovanju, izvršenju radova kompanija “Imperial Buildings” d.o.o. prati najsavremenije građevinske tokove i nudi rešenja koja su u skladu sa najzahtevnijim građevinskim standardima. Ciljevi u građevinarstvu su pre svega konstantno ulaganje u osavremenjivanje mehanizacije, usavršavanje kadrova radi pružanja mogućnosti za maksimalno korišćenje svih resursa firme. Izvođenje radova je u skladu sa zaštitom životne sredine i ljudskog zdravlja. Prilagođavamo se željama i potrebama svih potencijalnih saradnika, poštujemo rokove i obećavamo dobar kvalitet gradnje.</p>
					</div>
				</div>
            </div>
            
            <div class="reference reference--sm">
                <div class="wrapper wrapper--sm">
                    <div class="section-head">
                        <h2 class="section-head__title is-animate slide-fade">Reference</h2>
                        <h3 class="section-head__subtitle is-animate slide-fade" data-slide-delay="300">U našem dosadašnjem radu najzapaženije rezultate ostvarili smo na rekonstrukcijama vrelovodnih mreža, samoj izgradnji vrelovoda, izgradnji kanalizacione i vodovodne mreže..</h3>
                    </div>
                    <div class="reference__container">
                        <div class="reference__content-box-left">
                            <div class="reference__content-box  is-animate slide-fade" data-slide-delay="600">
                                <h3 class="reference__content-title">JKP Vodovod i kanalizacija, Novi Sad</h3>
                                <span class="reference__content-subtext">Rekonstrukcija vodovodne mreže</span>
                                <span class="reference__content-place">Beogradski kej, Novi Sad</span>
                            </div>

                            <div class="reference__content-box is-animate slide-fade" data-slide-delay="800">
                                <h3 class="reference__content-title">Vojvodina put, Bačka put</h3>
                                <span class="reference__content-subtext">Atmosferska kanalizacija</span>
                                <div class="referenc__content-text-wrap">
                                    <span class="reference__content-place">Temerinska ulica, Novi Sad</span>
                                </div>
                            </div>

                            <div class="reference__content-box is-animate slide-fade" data-slide-delay="1000">
                                <h3 class="reference__content-title">Bačka Palanka</h3>
                                <span class="reference__content-subtext">Sanacija kanala za odvod atmosferskih voda</span>
                                <div class="referenc__content-text-wrap">
                                    <span class="reference__content-place">Bačka Palanka</span>
                                </div>
                            </div>

                            <div class="reference__content-box is-animate slide-fade" data-slide-delay="1200">
                                <h3 class="reference__content-title">Aling-Conel d.o.o.</h3>
                                <span class="reference__content-subtext">Izvođenje radova na vrelovodnoj mreži</span>
                                <div class="referenc__content-text-wrap">
                                    <span class="reference__content-place">Urgentni centar, Novi Sad</span>
                                </div>
                            </div>
                            <div class="reference__content-box is-animate slide-fade"  data-slide-delay="1400">
                                <h3 class="reference__content-title">Millenium Team d.o.o.</h3>
                                <span class="reference__content-subtext">Kanalizaciona mreža</span>
                                <div class="referenc__content-text-wrap">
                                    <span class="reference__content-place">Žabalj</span>
                                </div>
                            </div>
                        </div>

                        <div class="reference__content-box-right">
                            <div class="reference__content-box is-animate slide-fade" data-slide-delay="600">
                                <h3 class="reference__content-title">JKP Novosadska toplana</h3>
                                <span class="reference__content-subtext">Izgradnja vrelovodne mreže</span>
                                <div class="referenc__content-text-wrap">
                                    <span class="reference__content-place">Kornelija Stankovića, Braće Mogin, Milana Glumca Stevana Mokranjca, Dr Đorđa Joanovića, Pasterova i Rumenački put Kineska četvrt Stevana Dejanova 3</span>
                                </div>
                            </div>
                        
                            <div class="reference__content-box is-animate slide-fade" data-slide-delay="800">
                                <h3 class="reference__content-title">Aling-Conel d.o.o.</h3>
                                <span class="reference__content-subtext">Izvođenje radova na vrelovodnoj mreži</span>
                                <div class="referenc__content-text-wrap">
                                    <span class="reference__content-place">Urgentni centar, Novi Sad</span>
                                </div>
                            </div>
                            <div class="reference__content-box is-animate slide-fade" data-slide-delay="1000">
                                <h3 class="reference__content-title">Millenium Team d.o.o.</h3>
                                <span class="reference__content-subtext">Kanalizaciona mreža</span>
                                <div class="referenc__content-text-wrap">
                                    <span class="reference__content-place">Žabalj</span>
                                </div>
                            </div>
                            
                            <div class="reference__content-box is-animate slide-fade"  data-slide-delay="1200">
                                <h3 class="reference__content-title">Vojvodina put, Bačka put</h3>
                                <span class="reference__content-subtext">Atmosferska kanalizacija</span>
                                <div class="referenc__content-text-wrap">
                                    <span class="reference__content-place">Temerinska ulica, Novi Sad</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="img-block">
				<div class="wrapper wrapper--sm">
					<div class="section-head">
						<h2 class="section-head__title is-animate slide-fade">Izvedeni radovi</h2>
						<h3 class="section-head__subtitle is-animate slide-fade" data-slide-delay="300">Izvođenje radova je u skladu sa zaštitom životne sredine i ljudskog zdravlja. 		Prilagođavamo se željama i potrebama svih potencijalnih saradnika, poštujemo rokove i obećavamo dobar kvalitet gradnje.</h3>
					</div>
                </div>
                
				<div class="img-block__container">
					<div class="img-block__image is-animate slide-fade">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/demo/img1.png" alt="">
					</div>
					<div class="img-block__image is-animate slide-fade"data-slide-delay="600">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/demo/img1.png" alt="">
					</div>
					<div class="img-block__image is-animate slide-fade"data-slide-delay="800">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/demo/img1.png" alt="">
					</div>
					<div class="img-block__image is-animate slide-fade"data-slide-delay="1000">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/demo/img1.png" alt="">
					</div>
					<div class="img-block__image is-animate slide-fade"data-slide-delay="1200">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/demo/img1.png" alt="">
					</div>
                </div>
                
				<div class="img-block__btn is-animate slide-fade"data-slide-delay="1400">
					<a class="btn btn--primary" href="javascript:;">Vidi galeriju</a>
                </div>
                
				<span class="img-block__text is-animate slide-fade" data-slide-delay="1600">U realizaciji Vaših projekata, odaberite Imperial Buildings</span>
            </div>
            
            <div class="text-block text-block--red">
				<div class="wrapper wrapper--sm">
					<div class="text-block__wrap">
						<h4 class="text-block__title is-animate slide-fade">Ulažemo u svoju budućnost</h4>
						<div class="text-block__txt is-animate slide-fade" data-slide-delay="500">
							<div class="entry-content">
								<p>Ako želite da postanete deo našeg tima, da sa Vama budemo još snažniji, bolji i uspešniji, pridružite nam se</p>
							</div>
						</div>
						<div class="text-block__btn is-animate slide-fade" data-slide-delay="700">
							<a class="btn btn--primary btn--primary-white" href="javascript:;">Saznaj više</a>
						</div>
					</div>
				</div>
			</div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
