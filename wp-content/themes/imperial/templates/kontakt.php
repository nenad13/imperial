<?php
/**
 * Template name: Kontakt
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package imperial
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<div class="banner banner--sm"style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/images/demo/banner2.png" alt="">)">
				<div class="banner__top">
					<div class="wrapper wrapper--sm">
						<div class="banner__content">
							<h1 class="banner__content-title is-animate slide-fade">Stojimo Vam</br><span class="underline"> na raspolaganju</span></h1>
						</div>
					</div>
				</div>
			</div>
			
			<div class="contact-info">
				<div class="contact-info__container">
					<div class="contact-info__box">
						<div class="contact-info__item">
							<span class="contact-info__title">Adresa</span>
							<span class="contact-info__subtext">Đorđa Zličića 22 21000 Novi Sad Republika Srbija</span>
						</div>

						<div class="contact-info__item">
							<span class="contact-info__title">Tel./fax</span>
							<span class="contact-info__subtext">
								<a href="tel:+381 21 420 448" >+381 21 420 448</a>
							</span>
						</div>

						<div class="contact-info__item">
							<span class="contact-info__title">Email</span>
							<span class="contact-info__subtext">
								<a href="mailto: office@imperialbuildings.rs">office@imperialbuildings.rs</a>
							</span>
						</div>
					</div>
					<div class="contact-info__image">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/mapa.png" alt="">
					</div>
				</div>
				<div class="contact-info__bottom">
					<div class="wrapper wrapper--sm">
						<div class="contact-info__bottom-container">
							<div class="contact-info__bottom-item">
								<span class="contact-info__title">PIB</span>
								<span class="contact-info__subtext">105098063</span>
							</div>
							<div class="contact-info__bottom-item">
								<span class="contact-info__title">Matični broj</span>
								<span class="contact-info__subtext">+381 21 420 448</span>
							</div>
							<div class="contact-info__bottom-item">
								<span class="contact-info__title">Šifra delatnosti</span>
								<span class="contact-info__subtext">4120</span>
							</div>
							<div class="contact-info__bottom-item">
								<span class="contact-info__title">EPPDV</span>
								<span class="contact-info__subtext">375355797</span>
							</div>
							<div class="contact-info__bottom-item">
								<span class="contact-info__title">Registarski broj</span>
								<span class="contact-info__subtext">8227462093</span>
							</div>
						</div>
					</div>
				</div>
		   </div>
			
            <div class="contact">
				<div class="contact__text-box">
					<div class="wrapper wrapper--sm">
						<div class="contact__text-box-wrap">
							<h2 class="contact__text-box-title">Pošaljite nam poruku</h2>
							<h3 class="contact__text-box-subtitle">Ukoliko želite nešto da nas pitate, pošaljite nam poruku, sa zadovoljstvom ćemo Vam odgovoriti.</h3>
						</div>
					</div>
				</div>
                <div class="wrapper wrapper--sm">
                    <form class="contact__form">
                        <div class="contact__item">
                            <label class="sr-only" for="fname">Ime i prezime</label>
							<input type="text" id="fname" name="name" placeholder="Ime i prezime *">
                        </div>
                        <div class="contact__item">
                            <label class="sr-only" for="e-mail">Ime i prezime</label>
							<input type="email" id="e-mail" name="email" placeholder="Email adresa *">
                        </div>
                        <div class="contact__item">
                            <label class="sr-only" for="object">Ime i prezime</label>
							<input type="text" id="object" name="object" placeholder="Predmet poruke *">
                        </div>
                        <div class="contact__item-textarea">
							<label class="sr-only" for="pismo">Poruka  *</label>
							<textarea id="pismo" name="pismo" placeholder=" Poruka *"></textarea>
                        </div>
                        <div class="contact__form-bottom">
                            <div class="contact__form-btn">
                                <button class="btn btn--primary-white">Pošalji</button>
                            </div>
                            <span>Obavezna polja označena su *</span>
                        </div>
                    </form>
                </div>
			</div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
