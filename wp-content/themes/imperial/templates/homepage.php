<?php
/**
 * Template name: Homepage
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package imperial
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<div class="banner" style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/images/demo/banner.png);">
				<div class="banner__top">
					<div class="wrapper wrapper--sm">
						<div class="banner__content">
							<h1 class="banner__content-title is-animate slide-fade">Dobrodošli na <span class="underline">Imperial Buildings</span></h1>
							<div class="banner__content-text is-animate slide-fade" data-slide-delay="400">
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Erat tortor ipsum mauris faucibus dignissim viverra mollis risus.</p>
							</div>
							<div class="banner__content-btns is-animate slide-fade" data-slide-delay="800">
								<a class="btn btn--primary" href="javascript:;">Traži ponudu</a>
								<a class="btn btn--ghost" href="javascript:;">Zakaži sastanak</a>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="intro">
				<div class="wrapper wrapper--sm">
					<div class="intro__container ">
						<div class="intro__box is-animate slide-left-xl">
							<span class="intro__box-number">12</span>
							<span class="intro__box-number-text">Godina iskustva</span>
						</div>
						<span class="intro__box-text is-animate slide-left-xl" data-slide-delay="500">Mi gradimo, mi menjamo budućnost.</span>
						<div class="intro__box-content is-animate slide-left-xl" data-slide-delay="800">
							<div class="entry-content">
								<p>Firma je osnovana 2007. godine u Novom Sadu kao društvo za inženjering, projektovanje, niskogradnju i visokogradnju. Izvodimo građevinske radove na izgradnji kanalizacione, vodovodne i gasne mreže zatim na izgradnji vrelovoda, kao i ostale građevinske radove.</p>
							</div>
							<div class="intro__box-content-btn is-animate slide-left-xl" data-slide-delay="1100">
								<a class="btn btn--primary" href="javascript:;">saznaj više</a>
							</div>
						</div>	
					</div>
				</div>
			</div>

			<div class="img-block img-block--sm">
				<div class="img-block__container">
					<div class="img-block__image js-carousel-item is-animate slide-fade">
						<img class="js-carousel-img" src="<?php echo get_template_directory_uri(); ?>/assets/images/demo/1.png" alt="">
						<img class="js-carousel-img" src="<?php echo get_template_directory_uri(); ?>/assets/images/demo/2.png" alt="">
						<img class="js-carousel-img" src="<?php echo get_template_directory_uri(); ?>/assets/images/demo/3.png" alt="">
						<img class="js-carousel-img" src="<?php echo get_template_directory_uri(); ?>/assets/images/demo/4.png" alt="">
					</div>
					<div class="img-block__image js-carousel-item is-animate slide-fade" data-slide-delay="400">
						<img class="js-carousel-img" src="<?php echo get_template_directory_uri(); ?>/assets/images/demo/1.png" alt="">
						<img class="js-carousel-img" src="<?php echo get_template_directory_uri(); ?>/assets/images/demo/2.png" alt="">
						<img class="js-carousel-img" src="<?php echo get_template_directory_uri(); ?>/assets/images/demo/3.png" alt="">
						<img class="js-carousel-img" src="<?php echo get_template_directory_uri(); ?>/assets/images/demo/4.png" alt="">
					</div>
					<div class="img-block__image js-carousel-item is-animate slide-fade" data-slide-delay="600">
						<img class="js-carousel-img" src="<?php echo get_template_directory_uri(); ?>/assets/images/demo/1.png" alt="">
						<img class="js-carousel-img" src="<?php echo get_template_directory_uri(); ?>/assets/images/demo/2.png" alt="">
						<img class="js-carousel-img" src="<?php echo get_template_directory_uri(); ?>/assets/images/demo/3.png" alt="">
						<img class="js-carousel-img" src="<?php echo get_template_directory_uri(); ?>/assets/images/demo/4.png" alt="">
					</div>
					<div class="img-block__image js-carousel-item is-animate slide-fade" data-slide-delay="800">
						<img class="js-carousel-img" src="<?php echo get_template_directory_uri(); ?>/assets/images/demo/1.png" alt="">
						<img class="js-carousel-img" src="<?php echo get_template_directory_uri(); ?>/assets/images/demo/2.png" alt="">
						<img class="js-carousel-img" src="<?php echo get_template_directory_uri(); ?>/assets/images/demo/3.png" alt="">
						<img class="js-carousel-img" src="<?php echo get_template_directory_uri(); ?>/assets/images/demo/4.png" alt="">
					</div>
					<div class="img-block__image js-carousel-item is-animate slide-fade" data-slide-delay="1000">
						<img class="js-carousel-img" src="<?php echo get_template_directory_uri(); ?>/assets/images/demo/1.png" alt="">
						<img class="js-carousel-img" src="<?php echo get_template_directory_uri(); ?>/assets/images/demo/2.png" alt="">
						<img class="js-carousel-img" src="<?php echo get_template_directory_uri(); ?>/assets/images/demo/3.png" alt="">
						<img class="js-carousel-img" src="<?php echo get_template_directory_uri(); ?>/assets/images/demo/4.png" alt="">
					</div>
				</div>
			</div>

			<div class="text-block">
				<div class="wrapper wrapper--sm">
					<div class="text-block__wrap">
						<p class="text-block__txt-strong is-animate slide-fade">Izvodimo građevinske radove na izgradnji kanalizacione,vodovodne i gasne mreže zatim na izgradnji vrelovoda, kao i ostale građevinske radove.</p>
						<div class="text-block__txt is-animate slide-fade" data-slide-delay="500">
							<div class="entry-content">
								<p>Posedujemo sopstvenu mehanizaciju, vozni park i tehničku opremljenost za obavljanje svih navedenih delatnosti. Preduzeće broji preko 70 zaposlenih, a uskladu sa potrebama angažuju se sezonski radnici.</p>
							</div>
						</div>
						<div class="text-block__btn is-animate slide-fade" data-slide-delay="700">
							<a class="btn btn--primary" href="javascript:;">Saznaj više</a>
						</div>
					</div>
					
				</div>
			</div>

			<div class="services">
				<div class="wrapper wrapper--sm">
					<div class="section-head">
						<h2 class="section-head__title is-animate slide-fade">Usluge</h2>
						<h3 class="section-head__subtitle is-animate slide-fade" data-slide-delay="300"> Naše preduzeće izvodi građevinske radove na izgradnji kanalizacione,vodovodne i gasne mreže zatim na izgradnji vrelovoda, kao i ostale građevinske radove.</h3>
					</div>
					<div class="services__container">
						<div class="service-item is-animate slide-fade">
							<h3 class="service-item__title">Izvođenje radova 5</h3>
							<div class="entry-content">
								<p>Naše preduzeće izvodi građevinske radove na izgradnji kanalizacione,vodovodne i gasne mreže zatim na izgradnji vrelovoda, kao i ostale građevinske radove.</p>
							</div>
						</div>
						<div class="service-item is-animate slide-fade" data-slide-delay="500">
							<h3 class="service-item__title">Izvođenje radova 5</h3>
							<div class="entry-content">
								<p>Naše preduzeće izvodi građevinske radove na izgradnji kanalizacione,vodovodne i gasne mreže zatim na izgradnji vrelovoda, kao i ostale građevinske radove.</p>
							</div>
						</div>
						<div class="service-item is-animate slide-fade" data-slide-delay="700">
							<h3 class="service-item__title">Izvođenje radova 5</h3>
							<div class="entry-content">
								<p>Naše preduzeće izvodi građevinske radove na izgradnji kanalizacione,vodovodne i gasne mreže zatim na izgradnji vrelovoda, kao i ostale građevinske radove.</p>
							</div>
						</div>
						<div class="service-item is-animate slide-fade" data-slide-delay="900">
							<h3 class="service-item__title">Izvođenje radova 5</h3>
							<div class="entry-content">
								<p>Naše preduzeće izvodi građevinske radove na izgradnji kanalizacione,vodovodne i gasne mreže zatim na izgradnji vrelovoda, kao i ostale građevinske radove.</p>
							</div>
						</div>
						<div class="service-item is-animate slide-fade" data-slide-delay="1100">
							<h3 class="service-item__title">Izvođenje radova 5</h3>
							<div class="entry-content">
								<p>Naše preduzeće izvodi građevinske radove na izgradnji kanalizacione,vodovodne i gasne mreže zatim na izgradnji vrelovoda, kao i ostale građevinske radove.</p>
							</div>
						</div>
						<div class="service-item is-animate slide-fade" data-slide-delay="1300">
							<h3 class="service-item__title">Izvođenje radova 5</h3>
							<div class="entry-content">
								<p>Naše preduzeće izvodi građevinske radove na izgradnji kanalizacione,vodovodne i gasne mreže zatim na izgradnji vrelovoda, kao i ostale građevinske radove.</p>
							</div>
						</div>
					</div>
					<div class="services__btn is-animate slide-fade" data-slide-delay="1500">
						<a class="btn btn--primary" href="javascript:;">saznaj više</a>
					</div>
				</div>
			</div>

			<div class="text-block">
				<div class="wrapper wrapper--sm">
					<div class="text-block__wrap">
						<p class="text-block__txt-strong is-animate slide-fade">Uspešno rešavamo sve zahteve investitora ma koliko oni bili komplikovani. Vodimo računa da idemo u korak sa vremenom, pratimo i upotrebljavamo najsavremenije materijale i tehnologije kako u projektovanju, tako i u niskogradnji, visokogradnji i svim ostalim građevinskim radovima.</p>
						<div class="text-block__txt is-animate slide-fade" data-slide-delay="500">
							<div class="entry-content">
								<p>Visoko profesionalni odnos zaposlenih, odanost i posvećenost prema radu, kvalitet usluga i stručnost kadra, poštovanje svih ugovorenih obaveza i rokova je razlog da svaki investitor ili naručioc posla bude siguran da ima pouzdanog i odgovornog izvođača.</p>
							</div>
						</div>
						<div class="text-block__btn is-animate slide-fade" data-slide-delay="700">
							<a class="btn btn--primary" href="javascript:;">saznaj više</a>
						</div>
					</div>
				</div>
			</div>

			<div class="reference">
				<div class="wrapper wrapper--sm">
					<div class="reference__container">
						<div class="reference__slider js-reference-slider">
							<div class="reference__image is-animate slide-fade">
								<img src="<?php echo get_template_directory_uri(); ?>/assets/images/demo/1.png" alt="">
							</div>
							<div class="reference__image is-animate slide-fade">
								<img src="<?php echo get_template_directory_uri(); ?>/assets/images/demo/2.png" alt="">
							</div>
							<div class="reference__image is-animate slide-fade">
								<img src="<?php echo get_template_directory_uri(); ?>/assets/images/demo/3.png" alt="">
							</div>
						</div>
						<!-- <div class="reference__image is-animate slide-fade">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/demo/15.png" alt="">
						</div> -->
						<div class="reference__content">
							<div class="section-head">
								<h2 class="section-head__title section-head__title--left is-animate slide-fade">Reference</h2>
							</div>
							<div class="reference__content-wrap">
								<div class="reference__content-item is-animate slide-fade" data-slide-delay="500">
									<h3 class="reference__content-title ">JKP Vodovod i kanalizacija, Novi Sad</h3>
									<span class="reference__content-subtext">Rekonstrukcija vodovodne mreže</span>
									<span class="reference__content-place">Beogradski kej, Novi Sad</span>
									
								</div>
								<div class="reference__content-item is-animate slide-fade" data-slide-delay="700">
									<h3 class="reference__content-title">Aling-Conel d.o.o.</h3>
									<span class="reference__content-subtext ">Izvođenje radova na vrelovodnoj mreži</span>
									<span class="reference__content-place">Urgentni centar, Novi Sad</span>
								</div>
								<div class="reference__content-item is-animate slide-fade" data-slide-delay="900">
									<h3 class="reference__content-title">Millenium Team d.o.o.</h3>
									<span class="reference__content-subtext">Kanalizaciona mreža</span>
									<span class="reference__content-place">Žabalj</span>
								</div>
								<div class="reference__content-item is-animate slide-fade"data-slide-delay="1100">
									<h3 class="reference__content-title">Vojvodina put, Bačka put</h3>
									<span class="reference__content-subtext">Atmosferska kanalizacija</span>
									<span class="reference__content-place">Temerinska ulica, Novi Sad</span>
								</div>
								<div class="reference__content-btn is-animate slide-fade"data-slide-delay="1300">
									<a class="btn btn--primary" href="javascript:;">Vidi sve</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="text-block text-block--red">
				<div class="wrapper wrapper--sm">
					<div class="text-block__wrap">
						<h4 class="text-block__title is-animate slide-fade">Ulažemo u svoju budućnost</h4>
						<div class="text-block__txt is-animate slide-fade" data-slide-delay="500">
							<div class="entry-content">
								<p>Ako želite da postanete deo našeg tima, da sa Vama budemo još snažniji, bolji i uspešniji, pridružite nam se</p>
							</div>
						</div>
						<div class="text-block__btn is-animate slide-fade" data-slide-delay="700">
							<a class="btn btn--primary btn--primary-white" href="javascript:;">Saznaj više</a>
						</div>
					</div>
				</div>
			</div>

			<div class="img-block">
				<div class="wrapper wrapper--sm">
					<div class="section-head">
						<h2 class="section-head__title is-animate slide-fade">Izvedeni radovi</h2>
						<h3 class="section-head__subtitle is-animate slide-fade" data-slide-delay="300">Izvođenje radova je u skladu sa zaštitom životne sredine i ljudskog zdravlja. 		Prilagođavamo se željama i potrebama svih potencijalnih saradnika, poštujemo rokove i obećavamo dobar kvalitet gradnje.</h3>
					</div>
				</div>
				<div class="img-block__container">
					<div class="img-block__image is-animate slide-fade"data-slide-delay="500">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/demo/img1.png" alt="">
					</div>
					<div class="img-block__image is-animate slide-fade" data-slide-delay="700">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/demo/img1.png" alt="">
					</div>
					<div class="img-block__image is-animate slide-fade" data-slide-delay="900">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/demo/img1.png" alt="">
					</div>
					<div class="img-block__image is-animate slide-fade" data-slide-delay="1100">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/demo/img1.png" alt="">
					</div>
					<div class="img-block__image is-animate slide-fade" data-slide-delay="1300">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/demo/img1.png" alt="">
					</div>
				</div>
				<div class="img-block__btn is-animate slide-fade" data-slide-delay="1500">
					<a class="btn btn--primary" href="javascript:;">Vidi galeriju</a>
				</div>
				<span class="img-block__text is-animate slide-fade" data-slide-delay="1700">U realizaciji Vaših projekata, odaberite Imperial Buildings</span>
			</div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
