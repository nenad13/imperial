<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package imperial
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<div class="main-content offset-top">
				<?php
					get_template_part( 'template-parts/modules/module', 'banner' );
				?>
				<div class="gallery">
					<div class="wrapper">
						<div class="gallery__container js-gallery-container">
							<?php
								while ( have_posts() ) : the_post(); ?>
									<?php 
									$images = get_field('gallery');
									if ( $images ) : ?>
										<?php foreach( $images as $image ) : ?>
											<div class="gallery__item is-animate slide-fade js-gallery-item">
												<a data-fancybox="gallery" href="<?php echo esc_url($image['url']); ?>">
													<img src="<?php echo esc_url($image['url']); ?>" alt="" />
												</a>
											</div>
										<?php endforeach; ?>
									<?php endif; ?>
								<?php endwhile; // End of the loop.
							?>
						</div>
						<div class="gallery-btn">
							<?php if ( have_rows('work_btn') ) : ?>
								<?php while ( have_rows('work_btn') ) : the_row(); ?>
									<a class="btn btn--primary" href="<?php echo get_sub_field('link'); ?>"><?php echo get_sub_field('label'); ?></a>
								<?php endwhile; ?>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();
