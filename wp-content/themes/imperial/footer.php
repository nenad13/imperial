<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package imperial
 */

?>

	</div><!-- #content -->
	
	<footer class="footer">
		<div class="wrapper wrapper--sm">
			<div class="footer__container">
				<div class="footer__logo">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
						<img src="<?php echo get_field('footer_logo', 'option')['url']; ?>" alt="">
					</a>
				</div>
				<div class="footer__info">
					<div class="footer__item-wrap">
						<div class="footer__item">
							<span><?php echo get_field('firm_name', 'option'); ?></span>
						</div>
						<div class="footer__item">
							<span><?php echo get_field('address', 'option'); ?></span>
						</div>
						<div class="footer__item">
							<a href="tel: <?php echo get_field('phone', 'option'); ?>"><span><?php _e('Telefon/fax: ', 'imperial'); ?><?php echo get_field('phone', 'option'); ?></span></a>
						</div>
						<div class="footer__item">
							<a href="mailto: <?php echo get_field('mail', 'option'); ?>"><span><?php echo get_field('mail', 'option'); ?></span></a>
						</div>
					</div>

					<div class="footer__bottom">
						<span><?php echo get_field('copy', 'option'); ?></span>
					</div>
				</div>
			</div>
		</div>
	</footer>

	
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
